﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cube.Api.Controllers
{
    [RoutePrefix("Admin")]
    public class AdminController : BaseApiController
    {

        [Authorize]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok();
        }
    }
}
