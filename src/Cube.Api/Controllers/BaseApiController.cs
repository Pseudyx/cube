﻿using System.Net.Http;
using System.Web.Http;
using Cube.Api.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Cube.Api.Controllers
{
    public class BaseApiController : ApiController
    {
        private DbManager _db = null;
        private ApplicationUserManager _AppUserManager = null;
        private AccountModelFactory _accModelFactory;
        private ApplicationRoleManager _AppRoleManager = null;
        private AuthRepository _authRepository = null;

        protected DbManager DbManager
        {
            get { return _db ?? Request.GetOwinContext().Get<DbManager>(); }
        }
        protected ApplicationUserManager AppUserManager
        {
            get
            {
                return _AppUserManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        protected ApplicationRoleManager AppRoleManager
        {
            get
            {
                return _AppRoleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        protected AccountModelFactory AccountModelFactory
        {
            get {
                return _accModelFactory ??
                       (_accModelFactory = new AccountModelFactory(this.Request, this.AppUserManager));
            }
        }

        protected AuthRepository AuthRepository
        {
            get
            {
                return _authRepository ?? (_authRepository = new AuthRepository());
            }
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}