﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Cube.Api.Models;

namespace Cube.Api.Controllers
{

    [AllowAnonymous]
    [RoutePrefix("RefreshTokens")]
    public class RefreshTokensController : BaseApiController
    {

        //[Authorize(Users = "Admin")]
        [AllowAnonymous]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(AuthRepository.GetAllRefreshTokens());
        }

        //[Authorize(Users = "Admin")]
        [AllowAnonymous]
        [Route("")]
        public async Task<IHttpActionResult> Delete(string tokenId)
        {
            var result = await AuthRepository.RemoveRefreshToken(tokenId);
            if (result)
            {
                return Ok();
            }
            return BadRequest("Token Id does not exist");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                AuthRepository.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
