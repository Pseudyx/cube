﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using Cube.Api.Entities;
using Cube.Api.Filters;
using Cube.Api.Models;
using Cube.Domain;
using Cube.Service;
using LinqKit;

namespace Cube.Api.Controllers
{
    [RoutePrefix("Snippet")]
    public class SnippetController : BaseApiController
    {
        [HttpPost]
        [Route("Add")]
        public async Task<IHttpActionResult> Add(SnippetViewModel newSnippetVm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = AuthRepository.FindByName(newSnippetVm.UserName);

                if (!string.IsNullOrEmpty(newSnippetVm.Text))
                {
                    var re = new Regex(@"(?<=#)\w+");
                    var matches = re.Matches(newSnippetVm.Text);
                    newSnippetVm.Tags = (from Match m in matches select m.Value).ToArray();
                }

                var newSnippet = new Snippet()
                {
                    Text = newSnippetVm.Text,
                    UserId = user.Id,
                    Created = (DateTime)newSnippetVm.Created,
                    Tags = (newSnippetVm.Tags != null) ? DbManager.SaveOrUpdateTagByRange(newSnippetVm.Tags) : null,
                    MediaTypeId = (!string.IsNullOrEmpty(newSnippetVm.MediaType)) ? (int)Enum.Parse(typeof(MediaTypes), newSnippetVm.MediaType) : 1,
                    CategoryId = (!string.IsNullOrEmpty(newSnippetVm.Category)) ? DbManager.SaveOrUpdateCategoryByName(newSnippetVm.Category).Id : 1,
                };
                DbManager.Snippets.Add(newSnippet);

                switch (newSnippet.MediaTypeId)
                {
                    case (int)MediaTypes.Link:
                        var snippetLink = new SnippetLink
                        {
                            Url = newSnippetVm.SnippetLink.Url,
                            Title = newSnippetVm.SnippetLink.Title,
                            Summary = newSnippetVm.SnippetLink.Summary,
                            Author = newSnippetVm.SnippetLink.Author,
                            Site = newSnippetVm.SnippetLink.Site,
                            Image = newSnippetVm.SnippetLink.Image,
                            Snippet = newSnippet
                        };
                        DbManager.SnippetLinks.Add(snippetLink);
                        break;
                    case (int)MediaTypes.Image:
                        var snippetImage = new SnippetImage
                        {
                            Name = newSnippetVm.SnippetImage.Name,
                            Extension = newSnippetVm.SnippetImage.Extension,
                            Image = newSnippetVm.SnippetImage.Image,
                            Snippet = newSnippet
                        };
                        DbManager.SnippetImages.Add(snippetImage);
                        break;
                    case (int)MediaTypes.Video:
                        var snippetVideo = new SnippetVideo
                        {
                            Url = newSnippetVm.SnippetVideo.Url,
                            Snippet = newSnippet
                        };
                        DbManager.SnippetVideos.Add(snippetVideo);
                        break;
                }

                //if (newSnippet.Tags != null) newSnippet.Tags.ToList().ForEach(tag => tag.Count++);

                
                await DbManager.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                return new System.Web.Http.Results.BadRequestErrorMessageResult(ex.ToString(), this);
            }
        }

        [HttpPost]
        [Route("Get")]
        public async Task<List<Snippet>> Get(PagedRequest<SnippetFilter> request)
        {
            var pred = PredicateBuilder.True<Snippet>();
            if (request.Filter.Category != null)
            {
                pred = PredicateBuilder.False<Snippet>();
                pred = pred.Or(s => s.Category.Name == request.Filter.Category);
            }

            if (request.Filter.Tags != null)
            {
                var tags = request.Filter.Tags.Split(',');
                pred = tags.Aggregate(pred, (current, tag) => current.And(s => s.Tags.Select(x => x.Name).Contains(tag.ToString().Trim())));
            }


            return await DbManager.Snippets
                .Include(p => p.User)
                .Include(p => p.MediaType)
                .Include(p => p.Category)
                .Include(p => p.SnippetLink)
                .Include(p => p.SnippetImage)
                .Include(p => p.SnippetVideo)
                .Include(p => p.Tags)
                .Include(p => p.Comments)
                .AsExpandable()
                .Where(pred)
                .OrderByDescending(x => x.Created)
                .Skip(request.Skip)
                .Take(request.Take)
                .ToListAsync();
        }

        [HttpGet]
        [Route("News")]
        public async Task<SnippetNewsViewModel> News()
        {
            var latestSnippet = 
                Get(new PagedRequest<SnippetFilter>
                {
                    Filter = new SnippetFilter {Category = null}, 
                    Take = 1
                }).Result.FirstOrDefault();

            var snippetCount = await DbManager.Snippets.CountAsync();

            return new SnippetNewsViewModel
            {
                SnippetCount = snippetCount,
                Snippet = latestSnippet,
                Comment = latestSnippet.Comments.FirstOrDefault()

            };
        }

        [HttpGet]
        [Route("Category/Get")]
        public async Task<List<Category>> CategoryGet()
        {
            return await DbManager.Categories.ToListAsync();
        }

        [HttpPost]
        [Route("Category/Add")]
        public async Task<IHttpActionResult> CategoryAdd(string category)
        {
            var cat = new Category
            {
                Name = category,
                Count = 0
            };

            DbManager.Categories.Add(cat);
            await DbManager.SaveChangesAsync();

            return Ok();
        }

        [HttpGet]
        [Route("ScrapeLink")]
        public async Task<LinkScrape> ScrapeLink(string link)
        {
            var scrapeSummary = PageScraper.ScrapeSummary(link);

            return scrapeSummary;
        }

        [HttpPost]
        [Route("Comment/Add")]
        public async Task<IHttpActionResult> Add(SnippetCommentViewModel newCommentVm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {

                var user = AuthRepository.FindByName(newCommentVm.UserName);

                var newComment = new SnippetComment
                {
                    SnippetId = newCommentVm.Id,
                    Text = newCommentVm.Text,
                    UserId = user.Id,
                    Created = (DateTime)newCommentVm.Created
                };
 
                DbManager.SnippetComments.Add(newComment);
                await DbManager.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                return new System.Web.Http.Results.BadRequestErrorMessageResult(ex.ToString(), this);
            }
        }

        [HttpGet]
        [Route("Comment/AllForSnippet")]
        public async Task<List<SnippetComment>> AllForSnippet(int snippetId)
        {
            return await DbManager.SnippetComments
                        .Include(i=>i.User)
                        .Where(x => x.SnippetId == snippetId)
                        .ToListAsync();
        }
    }

    
    
}
