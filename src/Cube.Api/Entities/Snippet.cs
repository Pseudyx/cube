﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Cube.Api.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cube.Api.Entities
{
    public class Snippet
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Text { get; set; }

        [Required]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        [Required]
        public DateTime Created { get; set; }
        public DateTime? Edited { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }

        public int MediaTypeId { get; set; }
        public virtual MediaType MediaType { get; set; }

        [Required]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public virtual ICollection<SnippetComment> Comments { get; set; }

        public virtual SnippetLink SnippetLink { get; set; }
        public virtual SnippetImage SnippetImage { get; set; }
        public virtual SnippetVideo SnippetVideo { get; set; }

    }

    public class SnippetComment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        [Required]
        public string Text { get; set; }
        
        [Required]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        [Required]
        public DateTime Created { get; set; }
        public DateTime? Edited { get; set; }

        [Required]
        public int SnippetId { get; set; }
        public Snippet Snippet { get; set; }
    }

    public class SnippetLink
    {
        public string Url { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Author { get; set; }
        public string Site { get; set; }
        public string Image { get; set; }

        [Key]
        public int SnippetId { get; set; }
        public virtual Snippet Snippet { get; set; }
    }

    public class SnippetImage
    {
        public string Name { get; set; }
        public string Extension { get; set; }
        public string Image { get; set; }

        [Key]
        public int SnippetId { get; set; }
        public virtual Snippet Snippet { get; set; }
    }

    public class SnippetVideo
    {
        public string Url { get; set; }

        [Key]
        public int SnippetId { get; set; }
        public virtual Snippet Snippet { get; set; }
    }

}