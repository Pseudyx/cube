﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cube.Api.Filters
{
    public class SnippetFilter
    {
        public string Category { get; set; }
        public string Tags { get; set; }
    }
}