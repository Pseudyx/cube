using System.Collections.Generic;
using Cube.Api.Entities;
using Cube.Api.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cube.Api.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Cube.Api.Models.DbManager>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DbManager context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.MediaTypes.AddOrUpdate(
                p => p.Name,
                new MediaType {Name = "Text"},
                new MediaType {Name = "Link"},
                new MediaType {Name = "Image"},
                new MediaType {Name = "Video"}
                );

            context.Categories.AddOrUpdate(
                p => p.Name,
                new Category {Name = "General"}
                );

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new DbManager()));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new DbManager()));

            var user = manager.FindByName("Cube");
            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = "Cube",
                    Email = "sempf83@gmail.com",
                    EmailConfirmed = true,
                    FirstName = "Cube",
                    LastName = "Admin",
                    JoinDate = DateTime.Now.AddYears(-3)
                };

                manager.Create(user, "cub3m1n");
            }

            if (!roleManager.Roles.Any())
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
                roleManager.Create(new IdentityRole { Name = "User" });
            }

            var adminUser = manager.FindByName("Cube");
            var managerRoles = manager.GetRoles(adminUser.Id);
            if (!managerRoles.Contains("Admin"))
            {
                manager.AddToRoles(adminUser.Id, new string[] { "Admin" });
            }

            if (!context.Clients.Any())
            {
                var clientsList = new List<Client>
                {
                    new Client
                    {
                        Id = "ngCubeApp",
                        Secret = Utility.GetHash("abc@123"),
                        Name = "AngularJS Application",
                        ApplicationType = Models.ApplicationTypes.JavaScript,
                        Active = true,
                        RefreshTokenLifeTime = 7200,
                        AllowedOrigin = "*"
                    }
                };

                context.Clients.AddRange(clientsList);
            }

            context.SaveChanges();
        }
    }
}
