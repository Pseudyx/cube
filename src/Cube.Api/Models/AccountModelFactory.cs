﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Routing;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cube.Api.Models
{
    public class AccountModelFactory
    {
        private UrlHelper _urlHelper;
        private ApplicationUserManager _authRepository;

         public AccountModelFactory(HttpRequestMessage request, ApplicationUserManager appUserManager)
        {
            _urlHelper = new UrlHelper(request);
            _authRepository = appUserManager;
        }

        public UserModel Create(ApplicationUser appUser)
        {
            return new UserModel
            {
                Url = _urlHelper.Link("GetUserById", new {id = appUser.Id}),
                Id = appUser.Id,
                UserName = appUser.UserName,
                FullName = string.Format("{0} {1}", appUser.FirstName, appUser.LastName),
                Email = appUser.Email,
                EmailConfirmed = appUser.EmailConfirmed,
                JoinDate = appUser.JoinDate,
                Avatar = appUser.Avatar,
                Roles = _authRepository.GetRolesAsync(appUser.Id).Result,
                Claims = _authRepository.GetClaimsAsync(appUser.Id).Result
            };
        }

        public ApplicationUser ModelToUser(ApplicationUser user, UserModel model)
        {
            user.Avatar = model.Avatar;
            user.Email = model.Email;

            return user;
        }

        public RoleModel Create(IdentityRole appRole)
        {

            return new RoleModel
            {
                Url = _urlHelper.Link("GetRoleById", new { id = appRole.Id }),
                Id = appRole.Id,
                Name = appRole.Name
            };
        }
    }

    public class UserModel
    {
        public string Url { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string Avatar { get; set; }
        public DateTime JoinDate { get; set; }
        public IList<string> Roles { get; set; }
        public IList<System.Security.Claims.Claim> Claims { get; set; }
    }

    public class RoleModel
    {
        public string Url { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }
}