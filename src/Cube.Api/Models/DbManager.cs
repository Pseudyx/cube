﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Cube.Api.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cube.Api.Models
{
    public class DbManager : IdentityDbContext<ApplicationUser>
    {
        public DbManager() : base("Cube", throwIfV1Schema: false)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static DbManager Create()
        {
            return new DbManager();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            //modelBuilder.Configurations.Add(new IdentityUserLoginConfiguration());
            //modelBuilder.Configurations.Add(new IdentityUserRoleConfiguration());

            modelBuilder.Entity<Snippet>()
                .HasMany(c => c.Tags).WithMany(i => i.Snippets)
                .Map(t => t.MapLeftKey("SnippetId")
                    .MapRightKey("TagId")
                    .ToTable("SnippetTag"));

            modelBuilder.Entity<Snippet>()
                .HasKey(x => x.Id)
                .HasOptional(x => x.SnippetLink)
                .WithRequired(x => x.Snippet)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Snippet>()
                .HasKey(x => x.Id)
                .HasOptional(x => x.SnippetImage)
                .WithRequired(x => x.Snippet)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Snippet>()
                .HasKey(x => x.Id)
                .HasOptional(x => x.SnippetVideo)
                .WithRequired(x => x.Snippet)
                .WillCascadeOnDelete(true);

            base.OnModelCreating(modelBuilder);

        }

        //Authenrication
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        //Indexes
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<MediaType> MediaTypes { get; set; }

        //Posts
        public DbSet<Snippet> Snippets { get; set; }
        public DbSet<SnippetLink> SnippetLinks { get; set; }
        public DbSet<SnippetImage> SnippetImages { get; set; }
        public DbSet<SnippetVideo> SnippetVideos { get; set; }
        public DbSet<SnippetComment> SnippetComments { get; set; }

        //TagManager
        public Tag SaveOrUpdateTagByName(string tagName)
        {
            var tag = Tags.FindByName(tagName);
            if (tag != null)
            {
                tag.Count++;
                return tag;
            }
                
            tag = new Tag()
            {
                Name = tagName,
                Count = 1
            };

            Tags.Add(tag);
            SaveChanges();

            return tag;
        }

        public ICollection<Tag> SaveOrUpdateTagByRange(string[] tagNames)
        {
            return tagNames.Select(SaveOrUpdateTagByName).ToList();
        }

        //Category Manager
        public Category SaveOrUpdateCategoryByName(string name)
        {
            var cat = Categories.FindByName(name);
            if (cat != null)
            {
                cat.Count++;
                return cat;
            }

            cat = new Category()
            {
                Name = name,
                Count = 1
            };

            Categories.Add(cat);
            SaveChanges();

            return cat;
        }

    }

    public static class DbManagerExtensions
    {
        public static Tag FindByName(this DbSet<Tag> Tags, string tagName)
        {
            return Tags.FirstOrDefault(x => x.Name == tagName);
        }

        public static MediaType FindByName(this DbSet<MediaType> MediaTypes, string typeName)
        {
            return MediaTypes.FirstOrDefault(x => x.Name == typeName);
        }

        public static Category FindByName(this DbSet<Category> Categories, string categoryName)
        {
            return Categories.FirstOrDefault(x => x.Name == categoryName);
        }

    }
}