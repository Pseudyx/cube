﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cube.Api.Models
{
    public enum ApplicationTypes
    {
        JavaScript = 0,
        NativeConfidential = 1
    };

    public enum Roles
    {
        user,
        admin
    }

    public enum MediaTypes
    {
        Text = 1,
        Link = 2,
        Image = 3,
        Video = 4
    }
}