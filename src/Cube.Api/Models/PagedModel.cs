﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cube.Api.Models
{
    public class PagedRequest<TFilter>
        where TFilter : class, new()
    {
        public TFilter Filter { get; set; }
        public int Take { get; set; }
        public int Skip { get; set; }

        public static PagedRequest<TFilter> Default
        {
            get
            {
                return new PagedRequest<TFilter>
                {
                    Take = int.MaxValue,
                    Filter = new TFilter()
                };
            }
        }
    }

    public class PagedResponse<TEntity>
        where TEntity : class
    {
        public int Total { get; set; }
        public TEntity[] Rows { get; set; }

        public PagedResponse()
        {
            Rows = new TEntity[0];
            Total = 0;
        } 
    }


}