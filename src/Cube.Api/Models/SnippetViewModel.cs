﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cube.Api.Entities;
using System.Runtime.Serialization;

namespace Cube.Api.Models
{
    public class SnippetViewModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public DateTime Created { get; set; }
        
        [DataMember]
        public DateTime Edited { get; set; }

        [DataMember]
        public virtual string[] Tags { get; set; }
        
        [DataMember]
        public string MediaType { get; set; }

        [DataMember]
        public string Category { get; set; }

        [DataMember]
        public SnippetLink SnippetLink { get; set; }

        [DataMember]
        public SnippetImage SnippetImage { get; set; }

        [DataMember]
        public SnippetVideo SnippetVideo { get; set; }

        
        [DataMember]
        public virtual ICollection<SnippetCommentViewModel> Comments { get; set; }

        

    }

    public class SnippetCommentViewModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public DateTime Created { get; set; }
        
        [DataMember]
        public DateTime Edited { get; set; }

    }

    public class SnippetNewsViewModel
    {
        public int SnippetCount { get; set; }
        public Snippet Snippet { get; set; }
        public SnippetComment Comment { get; set; }
    }
}