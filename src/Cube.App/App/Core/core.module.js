﻿cube.core = angular.module('cubeCore',
    [   'ngSanitize'
      , 'ui.router'
      , 'ui.bootstrap'
      , 'ui.select'
      , 'ngResource'
      , 'ngProgress'
      , 'ngAnimate'
      , 'LocalStorageModule'
      , 'angularMoment'
      , 'videoShare'
    ]);

cube.core.config([
    '$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/');

        $stateProvider.
            state('home', {
                url: '/',
                templateUrl: 'App/Core/home.html',
                controller: 'homeCtrl'
            }).
            state('signup', {
                url: '/signup',
                templateUrl: 'App/Core/signup.html',
                controller: 'signupCtrl'
            }).
            state('login', {
                url: '/login',
                templateUrl: 'App/Core/login.html',
                controller: 'loginCtrl'
            }).
            state("associate", {
                url: '/associate',
                templateUrl: "App/Core/associate.html",
                controller: "associateCtrl"
            }).
            state('profile', {
                url: '/profile',
                templateUrl: 'App/Core/profile.html',
                controller: 'profileCtrl',
                resolve: {
                    profile: function(authService, ngProgress) {
                        ngProgress.start();
                        return authService.GetUser(authService.authentication.userName);
                    }
                }
            });
    }
]);

cube.core.controller('signupCtrl', [
    '$scope', '$state', '$timeout', 'appSettings', 'authService',
    function ($scope, $state, $timeout, appSettings, authService) {

        $scope.savedSuccessfully = false;
        $scope.message = "";

        $scope.registration = {
            userName: "",
            password: "",
            confirmPassword: ""
        };

        $scope.signUp = function () {

            authService.saveRegistration($scope.registration).then(function (response) {

                $scope.savedSuccessfully = true;
                $scope.message = "User has been registered successfully, you will be redicted to login page in 2 seconds.";
                startTimer();

            },
             function (response) {
                 var errors = [];
                 for (var key in response.data.modelState) {
                     for (var i = 0; i < response.data.modelState[key].length; i++) {
                         errors.push(response.data.modelState[key][i]);
                     }
                 }
                 $scope.message = "Failed to register user due to:" + errors.join(' ');
             });
        };

        var startTimer = function () {
            var timer = $timeout(function () {
                $timeout.cancel(timer);
                $state.go('login');
            }, 2000);
        }

    }]);

cube.core.controller('loginCtrl', ['$scope', '$state', 'appSettings', 'authService', 'ngProgress',
    function ($scope, $state, appSettings, authService, ngProgress) {
        ngProgress.complete();

        $scope.loginData = {
            userName: "",
            password: ""
        };

        $scope.tester = function (message) {
            alert(message);
        }

        $scope.message = "";

        $scope.login = function () {

            authService.login($scope.loginData).then(function (response) {

                $state.go('cubed');

            },
             function (err) {
                 $scope.message = err.error_description;
             });
        };

        $scope.authExternalProvider = function (provider) {

            var redirectUri = location.protocol + '//' + location.host + '/authcomplete.html';

            var externalProviderUrl = appSettings.endpoints.apiUri + "/Account/ExternalLogin?provider=" + provider
                                                                        + "&response_type=token&client_id=" + appSettings.clientId
                                                                        + "&redirect_uri=" + redirectUri;
            window.$windowScope = $scope;

            var oauthWindow = window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
        };

        $scope.authCompletedCB = function (fragment) {

            $scope.$apply(function () {

                if (fragment.haslocalaccount == 'False') {

                    authService.logOut();

                    authService.externalAuthData = {
                        provider: fragment.provider,
                        userName: fragment.external_user_name,
                        externalAccessToken: fragment.external_access_token
                    };

                    $state.go('associate');

                }
                else {
                    //Obtain access token and redirect 
                    var externalData = { provider: fragment.provider, externalAccessToken: fragment.external_access_token };
                    authService.obtainAccessToken(externalData).then(function (response) {

                        $state.go('cubed');

                    },
                 function (err) {
                     $scope.message = err.error_description;
                 });
                }

            });
        }

    }]);

cube.core.controller('associateCtrl', [
    '$scope', '$state', '$timeout', 'authService',
    function ($scope, $state, $timeout, authService) {

        $scope.savedSuccessfully = false;
        $scope.message = "";

        $scope.registerData = {
            userName: authService.externalAuthData.userName,
            provider: authService.externalAuthData.provider,
            externalAccessToken: authService.externalAuthData.externalAccessToken
        };

        $scope.registerExternal = function () {

            authService.registerExternal($scope.registerData).then(function (response) {

                $scope.savedSuccessfully = true;
                $scope.message = "User has been registered successfully, you will be redicted in 2 seconds.";
                startTimer();

            },
              function (response) {
                  var errors = [];
                  for (var key in response.modelState) {
                      errors.push(response.modelState[key]);
                  }
                  $scope.message = "Failed to register user due to:" + errors.join(' ');
              });
        };

        var startTimer = function () {
            var timer = $timeout(function () {
                $timeout.cancel(timer);
                $state.go('admin');
            }, 2000);
        }

    }]);

cube.core.controller('profileCtrl', [
    '$scope', 'appSettings', 'ngProgress', 'authService', 'profile',
    function ($scope, appSettings, ngProgress, authService, profile) {
        ngProgress.complete();

        $scope.profile = profile;
        if ($scope.profile.avatar == null) {
            $scope.profile.avatar = "/Assets/Img/user-icon.png";
        }
        $scope.isImageUpdate = false;

        $scope.ImageUpdate = function () {
            $scope.isImageUpdate = !$scope.isImageUpdate;
        }

        $scope.uploadImage = function () {
            $scope.ImageUpdate();
            $scope.isImageDropped = false;
            $scope.SaveProfile();
        }

        $scope.dropEvent = function () {
            $scope.isImageDropped = true;
        }

        $scope.SaveProfile = function () {
            authService.SaveUser($scope.profile).then(function () {
                //do something after save profile
            });
        }
    }
]);

cube.core.controller("layoutCtrl", [
    '$scope', '$state', 'appSettings', 'authService', 'ngProgress',
    function ($scope, $state, appSettings, authService, ngProgress) {
        $scope.brand = appSettings.brand;
        $scope.copyright = new Date().getFullYear() + " " + $scope.brand.label;
        $scope.Nav = appSettings.navigation;
        $scope.isCollapsed = true;
        $scope.isPublic = function () {
            return $state.is('home') || $state.is('login') || $state.is('signup');
        }
        $scope.isCubed = function () {
            return $state.includes('cubed');
        }

        $scope.logOut = function () {
            authService.logOut();
            $state.go('home');
        }

        $scope.authentication = authService.authentication;

        ngProgress.color('#428bca');
        ngProgress.height('4px');

        $scope.Collapser = function () {
            $scope.isCollapsed = !($scope.isCollapsed);
        }
    }
]);

cube.core.controller('homeCtrl', [
    '$scope', 'appSettings',
    function ($scope, appSettings) {
        $scope.brand = appSettings.brand;
    }
]);

cube.core.run(['authService', 'ngProgress', function (authService, ngProgress) {
    authService.fillAuthData();
}]);