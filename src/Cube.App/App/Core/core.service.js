﻿cube.core.factory('authInterceptor', [
    '$q', '$injector', 'localStorageService',
    function ($q, $injector, localStorageService) {

        var authInterceptorServiceFactory = {};
        var $http;

        var _request = function (config) {

            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        }

        var _responseError = function (rejection) {
            var deferred = $q.defer();
            if (rejection.status === 401) {
                var authService = $injector.get('authService');
                authService.refreshToken().then(function (response) {
                    _retryHttpRequest(rejection.config, deferred);
                }, function () {
                    authService.logOut();
                    var $state = $injector.get('$state');
                    $state.go('login');
                    deferred.reject(rejection);
                });
            } else {
                deferred.reject(rejection);
            }
            return deferred.promise;
        }

        var _retryHttpRequest = function (config, deferred) {
            $http = $http || $injector.get('$http');
            $http(config).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
        }

        authInterceptorServiceFactory.request = _request;
        authInterceptorServiceFactory.responseError = _responseError;

        return authInterceptorServiceFactory;
    }]);

cube.core.service('authService', [
    '$q', '$injector', 'localStorageService', 'appSettings',
    function ($q, $injector, localStorageService, appSettings) {

        var serviceBase = appSettings.endpoints.apiUri;
        var _authentication = {
            isAuth: false,
            userName: "",
            useRefreshTokens: true
        };

        var _externalAuthData = {
            provider: "",
            userName: "",
            externalAccessToken: ""
        };

        var $http;

        var logOut = function () {

            localStorageService.remove('authorizationData');

            _authentication.isAuth = false;
            _authentication.userName = "";
            _authentication.useRefreshTokens = true;
        }

        return {
            authentication: _authentication,
            externalAuthData: _externalAuthData,

            logOut: logOut,

            saveRegistration: function (registration) {

                logOut();

                $http = $http || $injector.get('$http');
                return $http.post(serviceBase + '/account/register', registration).then(function (response) {
                    return response;
                });

            },

            login: function (loginData) {

                var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

                //if (loginData.useRefreshTokens) {
                data = data + "&client_id=" + appSettings.clientId;
                //}

                var deferred = $q.defer();

                $http = $http || $injector.get('$http');
                $http.post(serviceBase + '/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                    //if (loginData.useRefreshTokens) {
                    localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
                    //} else {
                    //    localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, refreshToken: "", useRefreshTokens: false });
                    //}
                    _authentication.isAuth = true;
                    _authentication.userName = loginData.userName;
                    _authentication.useRefreshTokens = true; //loginData.useRefreshTokens;

                    deferred.resolve(response);

                }).error(function (err, status) {
                    logOut();
                    deferred.reject(err);
                });

                return deferred.promise;

            },

            fillAuthData: function () {

                var authData = localStorageService.get('authorizationData');
                if (authData) {
                    _authentication.isAuth = true;
                    _authentication.userName = authData.userName;
                    _authentication.useRefreshTokens = authData.useRefreshTokens;
                }

            },

            refreshToken: function () {
                var deferred = $q.defer();

                var authData = localStorageService.get('authorizationData');

                if (authData && authData.useRefreshTokens) {

                    var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + appSettings.clientId;

                    localStorageService.remove('authorizationData');

                    $http = $http || $injector.get('$http');
                    $http.post(serviceBase + '/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                        localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });

                        deferred.resolve(response);

                    }).error(function (err, status) {
                        logOut();
                        deferred.reject(err);
                    });
                } else {
                    deferred.reject();
                }

                return deferred.promise;
            },

            obtainAccessToken: function (externalData) {

                var deferred = $q.defer();

                $http = $http || $injector.get('$http');
                $http.get(serviceBase + '/account/ObtainLocalAccessToken', { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } }).success(function (response) {

                    localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

                    _authentication.isAuth = true;
                    _authentication.userName = response.userName;
                    _authentication.useRefreshTokens = false;

                    deferred.resolve(response);

                }).error(function (err, status) {
                    logOut();
                    deferred.reject(err);
                });

                return deferred.promise;

            },

            registerExternal: function (registerExternalData) {

                var deferred = $q.defer();

                $http = $http || $injector.get('$http');
                $http.post(serviceBase + '/account/registerexternal', registerExternalData).success(function (response) {

                    localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });

                    _authentication.isAuth = true;
                    _authentication.userName = response.userName;
                    _authentication.useRefreshTokens = false;

                    deferred.resolve(response);

                }).error(function (err, status) {
                    logOut();
                    deferred.reject(err);
                });

                return deferred.promise;
            },

            GetUsers: function () {
                var deferred = $q.defer();
                $http({ method: 'GET', url: appSettings.endpoints.apiUri + '/Account/Users' }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },

            GetUser: function (userName) {
                var deferred = $q.defer();
                $http({ method: 'GET', url: appSettings.endpoints.apiUri + '/Account/user/' + userName }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },

            SaveUser: function (user) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: appSettings.endpoints.apiUri + '/Account/user/save', data: user }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },

            auth: function () {
                var deferred = $q.defer();
                $http = $http || $injector.get('$http');
                $http({ method: 'GET', url: appSettings.endpoints.apiUri + '/Account/' }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }
        }
    }
]);

cube.core.service('tokensManagerService', [
    '$http', 'appSettings',
    function ($http, appSettings) {

        var serviceBase = appSettings.endpoints.apiUri;

        return {

            getRefreshTokens: function () {
                return $http.get(serviceBase + '/refreshtokens').then(function (results) {
                    return results;
                });
            },

            deleteRefreshTokens: function (tokenid) {
                return $http.delete(serviceBase + '/refreshtokens/?tokenid=' + tokenid).then(function (results) {
                    return results;
                });
            }

        }
    }]);