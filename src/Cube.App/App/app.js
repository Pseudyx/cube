﻿var cube = {
    App: angular.module('cubeApp', [
        'cubeCore'
      , 'adminModule'
      , 'cubedModule'
    ])
    .constant('appSettings', {
        brand: {
            label: "Cube"
        },

        endpoints: {
            apiUri: 'https://localhost:44300'
        },

        clientId: 'ngCubeApp',

        navigation: {
            mainNav: [], //Nav items are added at the module
            footerNav: []
        }
    })
}
