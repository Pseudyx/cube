﻿cube.App.filter('getById', function () {
	return function (input, id) {
		var i = 0, len = input.length;
		for (; i < len; i++) {
			if (+input[i].id == +id) {
				return input[i];
			}
		}
		return null;
	}
});

if (typeof (String.prototype.trim) === "undefined") {
	String.prototype.trim = function () {
		return String(this).replace(/^\s+|\s+$/g, '');
	};
}