﻿cube.adminModule = angular.module('adminModule', []);

cube.adminModule.run(['appSettings', function (appSettings) {
    appSettings.navigation.mainNav.push({ label: "Admin", state: "admin", auth: true });
}]);

cube.adminModule.config([
    '$stateProvider', 
    function ($stateProvider) {
        $stateProvider.
            state('admin', {
                url: '/admin',
                templateUrl: 'App/components/admin/index.html',
                controller: 'adminCtrl',
                resolve: {
                    auth: function (authService, ngProgress) {
                        ngProgress.start();
                        return authService.auth();
                    }
                }
            }).
            state('admin_refresh', {
                url: '/admin/refresh',
                templateUrl: 'App/components/admin/refresh.html',
                controller: 'refreshCtrl'
            }).
            state('admin_tokens', {
                url: '/admin/tokens',
                templateUrl: 'App/components/admin/tokens.html',
                controller: 'tokensManagerCtrl'
            });
    }
]);

cube.adminModule.controller('adminCtrl', [
    '$scope', '$state', 'appSettings', 'ngProgress', 'authService',
    function ($scope, $state, appSettings, ngProgress, authService) {
        ngProgress.complete();

       $scope.goto = function () {
           $state.go('3.snippet');
       }

        authService.GetUsers().then(function(response) {
            $scope.users = response;
        });
    }
]);

cube.adminModule.controller('refreshCtrl', [
    '$scope', '$state', 'authService',
    function ($scope, $state, authService) {

    $scope.authentication = authService.authentication;
    $scope.tokenRefreshed = false;
    $scope.tokenResponse = null;

    $scope.refreshToken = function () {

        authService.refreshToken().then(function (response) {
            $scope.tokenRefreshed = true;
            $scope.tokenResponse = response;
        },
         function (err) {
             $state.go('login');
         });
    };

    }]);

cube.adminModule.controller('tokensManagerCtrl', [
    '$scope', 'tokensManagerService',
    function ($scope, tokensManagerService) {

    $scope.refreshTokens = [];

    tokensManagerService.getRefreshTokens().then(function (results) {

        $scope.refreshTokens = results.data;

    }, function (error) {
        alert(error.data.message);
    });

    $scope.deleteRefreshTokens = function (index, tokenid) {

        tokenid = window.encodeURIComponent(tokenid);

        tokensManagerService.deleteRefreshTokens(tokenid).then(function (results) {

            $scope.refreshTokens.splice(index, 1);

        }, function (error) {
            alert(error.data.message);
        });
    }

}]);