﻿cube.adminModule.service('adminApiService', [
    '$http', '$q', 'appSettings',
    function ($http, $q, appSettings) {
        return {
            auth: function () {
                var deferred = $q.defer();
                $http({ method: 'GET', url: appSettings.endpoints.apiUri + '/Admin/' }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }
        }
    }
]);