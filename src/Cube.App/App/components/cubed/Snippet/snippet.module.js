﻿cube.cubedModule.snippetModule = angular.module('snippetModule', []);

cube.cubedModule.run(['appSettings', function (appSettings) {
    appSettings.navigation.mainNav.push({ label: "Snippet", state: "snippet", auth: true });
}]);

cube.cubedModule.snippetModule.config([
    '$stateProvider',
    function ($stateProvider) {
        $stateProvider.state('cubed.snippet', {
            templateUrl: 'App/Components/Cubed/Snippet/snippets.html',
                controller: 'snippetCtrl',
                resolve: {
                    snippets: function (snippetApiService, ngProgress) {
                        ngProgress.start();
                        var request = {
                            take: 9,
                            skip: 0,
                            filter: {
                                category: null
                            }
                        }
                        return snippetApiService.Get(request);
                    }
                }
            }).
            state('snippet', {
                url: '/snippet',
                templateUrl: 'App/Components/Cubed/Snippet/snippets.html',
                controller: 'snippetCtrl',
                resolve: {
                    snippets: function (snippetApiService, ngProgress) {
                        ngProgress.start();
                        var request = {
                            take: 9,
                            skip: 0,
                            filter: {
                                category: null
                            }
                        }
                        return snippetApiService.Get(request);
                    }
                }
            });
    }
]);

cube.cubedModule.snippetModule.controller('snippetCtrl', [
    '$scope','$http', '$filter', '$state', 'appSettings', 'authService', 'ngProgress', 'snippetApiService', 'snippets',
    function ($scope, $http, $filter, $state, appSettings, authService, ngProgress, snippetApiService, snippets) {
        //
        var rowCalc = function(arr) {
            var all = [].concat(arr);
            var col1 = [];
            var col2 = [];
            var col3 = [];
            while (arr.length > 0) {
                var chunk = arr.splice(0, 3);
                if (chunk[0] != null) col1.push(chunk[0]);
                if (chunk[1] != null) col2.push(chunk[1]);
                if (chunk[2] != null) col3.push(chunk[2]);
            }

            return {
                all: all,
                col1: col1,
                col2: col2,
                col3: col3
            }
        };

        //function to load snippetes using request var
        var getSnippets = function() {
            snippetApiService.Get($scope.request).then(function(snippets) {
                $scope.snippets = rowCalc(snippets);
            }, function() { alert('error while fetching data from server'); });
        };

        var getCategories = function() {
            snippetApiService.Categories.Get().then(function(categories) {
                $scope.category = {
                    names: _.pluck(categories, "name"),
                    objects: categories
            }
            });
        };

        if (snippets) {
            //load in snippets fromstate resolve 
            //and set loading bar complete
            ngProgress.complete();
            $scope.snippets = rowCalc(snippets);

            //load categories
            getCategories();

            snippetApiService.SnippetNews().then(function (news) {
                $scope.SnippetNews = news;
            });
        };

        $scope.isDashboard = !$state.includes('snippet');
        

        //set initial vars
        $scope.request = {
            take: 9,
            skip: 0,
            filter: {
                category: null,
                tags: null
            }
        }

        $scope.snippetbox = '';
        $scope.snippetFocus = false;
        $scope.newSnippet = {
            UserName: authService.authentication.userName,
            MediaType: 'Text',
            Text: '',
            Category: 'General'
        };
        $scope.attachLink = {
            url: '',
            images: null,
            title: '',
            summary: '',
            author: '',
            site: ''
        };
        $scope.attachImage = {
            image: null,
            fileName: '',
        };
        $scope.attachVideo = {
            url: '',
            display: false
        };

        $scope.snippetComment = [];

        //Select media type links
        $scope.textFocus = function() {
            $scope.snippetFocus = true;
            $scope.snippetbox = 'snippetbox-expand';
        }
        $scope.SelectText = function() {
            $scope.newSnippet.MediaType = "Text";
            $scope.textFocus();
        }
        $scope.SelectLink = function() {
            $scope.newSnippet.MediaType = "Link";
            $scope.textFocus();
        }
        $scope.SelectImage = function() {
            $scope.newSnippet.MediaType = "Image";
            $scope.textFocus();
        }
        $scope.SelectVideo = function() {
            $scope.newSnippet.MediaType = "Video";
            $scope.textFocus();
        }

        $scope.clickTag = function(tag) {
            $scope.request.filter.tags = tag;
            getSnippets();
        }

        //categories
        $scope.GetCategory = function(category) {
            $scope.request.filter.category = category;
            $scope.request.skip = 0;
            getSnippets();
        }
        $scope.AddCategory = function(item) {
            if (($scope.categories.indexOf(item) == -1) && !(_.isNull(item) || _.isUndefined(item) || _.isEmpty(item))) {                
                    $scope.categories.push(item);
            }
        }

        //Add snippet to database
        $scope.AddSnippet = function () {
            //set created date now,instead of 
            //when page loads for acuracy
            $scope.newSnippet.Created = new Date();

            //check media type = link and create 
            //snippetLink from page
            if ($scope.newSnippet.MediaType == "Link") {
                $scope.newSnippet.SnippetLink = {
                    Url: $scope.attachLink.url,
                    Title: $scope.attachLink.title,
                    Summary: $scope.attachLink.summary,
                    Author: $scope.attachLink.author,
                    Site: $scope.attachLink.site, 
                    Image: $scope.attachLink.images[$scope.attachLink.images.i]
                }
            }

            //check media type = image and create 
            //snippetImage from page
            if ($scope.newSnippet.MediaType == "Image") {
                var re = /(?:\.([^.]+))?$/;
                $scope.newSnippet.SnippetImage = {
                    Name: $scope.attachImage.fileName,
                    Extension: re.exec($scope.attachImage.fileName)[1], 
                    Image: $scope.attachImage.image
                }
            }

            //check media type = image and create 
            //snippetImage from page
            if ($scope.newSnippet.MediaType == "Video") {
                $scope.newSnippet.SnippetVideo = {
                    Url: $scope.attachVideo.url
                }
            }

            //Call ADD in snippet api service to POST data to server
            snippetApiService.Add($scope.newSnippet).then(function () {
                //after postdata sucess, get all snippets again
                //and clear snippet box
                getSnippets();
                getCategories();
                $scope.ClearSnippet();
            }, function () {
                alert('error while adding snippet at server');
            });
        }

        //clear all values
        $scope.ClearSnippet = function () {
            $scope.snippetbox = '';
            $scope.snippetFocus = false;
            $scope.newSnippet.Text = '';
            $scope.newSnippet.MediaType = "Text";
            $scope.attachLink.title = '';
            $scope.attachLink.summary = '';
            $scope.attachLink.images = ['//:0'];
            $scope.attachLink.images.i = 0;
            $scope.attachLink.url = '';
            $scope.attachLink.author = '';
            $scope.attachLink.site = '';
            $scope.attachImage.image = '//:0';
            $scope.attachImage.fileName = '';
            $scope.attachVideo.url = '';
            $scope.attachVideo.display = false;
        }

        //call snippet api service to get summary from scraping page of given url
        $scope.checkLink = function () {
            if ($scope.attachLink.url != '')
            snippetApiService.ScrapeLink($scope.attachLink.url).then(function (response) {
                $scope.attachLink = response;
                $scope.attachLink.images.i = 0;
                },
         function (err) {
             var test = err;
         });
        }
        $scope.linkImgIndex = function (intrement) {
            var inc = $scope.attachLink.images.i + intrement;
            if (inc >= $scope.attachLink.images.length) inc = 0;
            if (inc < 0) inc = $scope.attachLink.images.length;
            $scope.attachLink.images.i = inc;
        }
        $scope.checkVideo = function () {
            if ($scope.attachVideo.url != '')
            $scope.attachVideo.display = true;
        }

        $scope.loadMore = function() {
            $scope.request.skip += 9;
            snippetApiService.Get($scope.request).then(function(snippets) {
                if (snippets.length > 0) {
                    var newSnipps = rowCalc(snippets);
                    [].push.apply($scope.snippets.all, newSnipps.all);
                    [].push.apply($scope.snippets.col1, newSnipps.col1);
                    [].push.apply($scope.snippets.col2, newSnipps.col2);
                    [].push.apply($scope.snippets.col3, newSnipps.col3);
                }
            }, function () { alert('error while fetching data from server'); });
        
        }

        $scope.activeNewComment = [];
        $scope.commentFocus =function(id) {
            $scope.activeNewComment.push(id);
        }

        $scope.AddComment= function(id) {
            snippetApiService.AddComment({
                Id: id,
                Text: $scope.snippetComment[id].comment.trim(),
                UserName: authService.authentication.userName,
                Created: new Date()
            }).then(function () {
                //after postdata sucess, get all snippets again
                //and clear snippet box
                snippetApiService.SnippetComments(id).then(function(comments) {
                    $filter('getById')($scope.snippets.all, id).comments = comments;
                    $scope.ClearComment(id);
                }, function() {
                    alert('error while adding snippet at server');
                });
            }, function () {
                alert('error while adding snippet at server');
            });
        }

        $scope.ClearComment = function (id) {
            $scope.snippetComment[id].comment = '';

            var ix = $scope.activeNewComment.indexOf(id);
            $scope.activeNewComment.splice(ix, 1);
        }
    }
]);