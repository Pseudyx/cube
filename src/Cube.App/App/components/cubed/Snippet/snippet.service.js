﻿cube.App.service('snippetApiService', [
'$http', '$q', 'appSettings',
function ($http, $q, appSettings) {
    return {
        Get: function (request) {
            var deferred = $q.defer();
            $http({ method: 'POST', url: appSettings.endpoints.apiUri + '/Snippet/Get', data: request }).success(deferred.resolve).error(deferred.reject);
            return deferred.promise;
        },
        Categories: {
            Get: function () {
                var deferred = $q.defer();
                $http({ method: 'GET', url: appSettings.endpoints.apiUri + '/Snippet/Category/Get' }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            },
            Add: function (category) {
                var deferred = $q.defer();
                $http({ method: 'POST', url: appSettings.endpoints.apiUri + '/Snippet/Category/Add', data: category }).success(deferred.resolve).error(deferred.reject);
                return deferred.promise;
            }
        },
        Add: function (snippet) {
            var deferred = $q.defer();
            $http({ method: 'POST', url: appSettings.endpoints.apiUri + '/Snippet/Add', data: snippet }).success(deferred.resolve).error(deferred.reject);
            return deferred.promise;
        },
        ScrapeLink: function (link) {
            var deferred = $q.defer();
            $http({ method: 'GET', url: appSettings.endpoints.apiUri + '/Snippet/ScrapeLink/?link=' + link }).success(deferred.resolve).error(deferred.reject);
            return deferred.promise;
        },
        AddComment: function (comment) {
            var deferred = $q.defer();
            $http({ method: 'POST', url: appSettings.endpoints.apiUri + '/Snippet/Comment/Add', data: comment }).success(deferred.resolve).error(deferred.reject);
            return deferred.promise;
        },
        SnippetComments: function (snippetId) {
            var deferred = $q.defer();
            $http({ method: 'GET', url: appSettings.endpoints.apiUri + '/Snippet/Comment/AllForSnippet', params: { snippetId: snippetId } }).success(deferred.resolve).error(deferred.reject);
            return deferred.promise;
        },
        SnippetNews: function () {
            var deferred = $q.defer();
            $http({ method: 'GET', url: appSettings.endpoints.apiUri + '/Snippet/News' }).success(deferred.resolve).error(deferred.reject);
            return deferred.promise;
        },
    }
}
]);