﻿cube.cubedModule = angular.module('cubedModule',
    [
        'snippetModule'
    ]);

cube.cubedModule.config([
    '$stateProvider',
    function($stateProvider) {
        $stateProvider.state('cubed', {
            url: '/cubed',
            templateUrl: 'App/Components/Cubed/index.html',
            controller: 'cubedCtrl',
            resolve: {
                auth: function(authService, ngProgress) {
                    ngProgress.start();
                    return authService.auth();
                }
            }
        });
    }
]);

cube.cubedModule.controller('cubedCtrl', [
    '$scope', 'appSettings', 'ngProgress', 'snippetApiService',
    function($scope, appSettings, ngProgress, snippetApiService) {
        ngProgress.complete();

        snippetApiService.SnippetNews().then(function (news) {
            $scope.SnippetNews = news;
        });


    }
]);

