﻿cube.videoShare = angular.module('videoShare', []);
cube.videoShare.directive('embedVideo', ['$filter', 'RegisteredPlayers', '$sce', function ($filter, RegisteredPlayers, $sce) {
    'use strict';
    return {
        restrict: "E",
        template: '<iframe data-ng-attr-id="{{id}} "width="{{width}}" height="{{height}}" data-ng-src="{{trustedVideoSrc}}" allowfullscreen frameborder="0"></iframe>',
        scope: {
            height: '@',
            width: '@'
        },
        link: function ($scope, $element, $attrs) {
            $attrs.$observe('width', function (w) {
                $scope.width = w;
            });

            $attrs.$observe('height', function (h) {
                $scope.height = h;
            });

            $attrs.$observe('iframeId', function (id) {
                $scope.id = id;
            });

            //handle the use of both ng-href and href
            $attrs.$observe('href', function (url) {
                if (url === undefined) {
                    return;
                }

                var player = null;
                //search for the right player in the list of registered players
                angular.forEach(RegisteredPlayers, function (value) {
                    if (value.isPlayerFromURL(url)) {
                        player = value;
                    }
                });

                if (player === null) {
                    return; //haven't found a match for a valid registered player
                }

                var videoID = url.match(player.playerRegExp)[1],
                    time = url.match(player.timeRegExp),
                    config = player.config;

                //overwrite playback options
                angular.forEach($filter('whitelist')($attrs, player.whitelist), function (value, key) {
                    var normalizedKey = config.transformAttrMap[key] != undefined ? config.transformAttrMap[key] : key;
                    config.settings[normalizedKey] = value;
                });

                config.settings.start = 0;

                if (time) {
                    switch (player.type) {
                        case "youtube":
                            config.settings.start += (parseInt(time[2] || "0") * 60 * 60);
                            config.settings.start += (parseInt(time[4] || "0") * 60);
                            config.settings.start += (parseInt(time[6] || "0"));
                            break;

                        case "dailymotion":
                            config.settings.start += (parseInt(time[1] || "0"));
                            break;

                        default:
                            break;
                    }
                }

                //process the settings for each player
                var settings = player.processSettings(config.settings, videoID);

                //build and trust the video URL
                var untrustedVideoSrc = '//' + config.playerID + videoID + $filter('videoSettings')(settings);
                $scope.trustedVideoSrc = $sce.trustAsResourceUrl(untrustedVideoSrc);
            });
        }
    }
}]);
cube.videoShare.service('PlayerConfig', function () {
    'use strict';
    this.createInstance = function (init) {
        var PlayerConfig = function (init) {
            this.type = init.type;
            this.playerRegExp = init.playerRegExp;
            this.timeRegExp = init.timeRegExp;
            this.whitelist = init.whitelist;
            this.config = {
                playerID: init.playerID,
                settings: init.settings,
                transformAttrMap: init.transformAttrMap
            };
            this.processSettings = init.processSettings;
            this.isPlayerFromURL = function (url) {
                return (url.match(this.playerRegExp) != null);
            }
        };
        return new PlayerConfig(init);
    }
});
cube.videoShare.factory('RegisteredPlayers', ['PlayerConfig', function (PlayerConfig) {
    'use strict';
    var configurations = {
        youtube: {
            type: "youtube",
            settings: {
                autoplay: 0,
                controls: 1,
                loop: 0
            },
            whitelist: ['autohide', 'cc_load_policy', 'color', 'disablekb', 'enablejsapi',
                'autoplay', 'controls', 'loop', 'playlist', 'rel', 'wmode', 'start', 'showinfo',
                'end', 'fs', 'hl', 'iv_load_policy', 'list', 'listType', 'modestbranding', 'origin',
                'playerapiid', 'playsinline', 'theme'],
            transformAttrMap: {},
            processSettings: function (settings, videoID) {
                if (settings['loop'] == 1 && (settings['playlist'] == undefined)) {
                    settings['playlist'] = videoID;
                }
                return settings;
            },
            playerID: 'www.youtube.com/embed/',
            protocol: 'http://',
            playerRegExp: /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/,
            timeRegExp: /t=(([0-9]+)h)?(([0-9]{1,2})m)?(([0-9]+)s?)?/
        },
        vimeo: {
            type: "vimeo",
            settings: {
                autoplay: 0,
                loop: 0,
                api: 0,
                player_id: ''
            },
            whitelist: ['autoplay', 'autopause', 'badge', 'byline', 'color', 'portrait', 'loop', 'api',
                'playerId', 'title'],
            transformAttrMap: { 'playerId': 'player_id' },
            processSettings: function (settings, videoID) {
                return settings;
            },
            playerID: 'player.vimeo.com/video/',
            protocol: 'http://',
            playerRegExp: /vimeo\.com\/([A-Za-z0-9]+)/,
            timeRegExp: ''
        },
        dailymotion: {
            type: "dailymotion",
            settings: {
                autoPlay: 0,
                logo: 0
            },
            whitelist: ['api', 'autoPlay', 'background', 'chromeless', 'controls', 'foreground', 'highlight', 'html',
                'id', 'info', 'logo', 'network', 'quality', 'related', 'startscreen', 'webkit-playsinline', 'syndication'],
            transformAttrMap: {},
            processSettings: function (settings, videoID) {
                return settings;
            },
            playerID: 'www.dailymotion.com/embed/video/',
            protocol: 'http://',
            playerRegExp: /www\.dailymotion\.com\/video\/([A-Za-z0-9]+)/,
            timeRegExp: /start=([0-9]+)/
        },
        youku: {
            type: "youku",
            settings: {},
            whitelist: [],
            transformAttrMap: {},
            processSettings: function (settings, videoID) {
                return settings;
            },
            playerID: 'player.youku.com/embed/',
            protocol: 'http://',
            playerRegExp: /youku\.com\/v_show\/id_([A-Za-z0-9]+).html/,
            timeRegExp: ''
        }
    };
    var players = [];
    angular.forEach(configurations, function (value) {
        players.push(PlayerConfig.createInstance(value));
    });
    return players;
}]);
cube.videoShare.filter('videoSettings', function () {
    'use strict';
    return function (settings) {
        var params = [];
        angular.forEach(settings, function (value, key) {
            params.push([key, value].join('='));
        });
        return params.length > 0 ? "?" + params.join('&') : "";
    }
});
cube.videoShare.filter('whitelist', function () {
    'use strict';
    return function (options, whitelist) {
        var filteredOptions = {};
        angular.forEach(options, function (value, key) {
            if (whitelist.indexOf(key) != -1) filteredOptions[key] = value;
        });
        return filteredOptions;
    }
});