﻿window.alert = function (message) {
    swal(message);
}

window.confirm = function (message, callback) {
    swal(
        {
            title: "Are you sure?",
            text: message,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                swal("Confirmed!", "Your action is being processed.", "success");
                callback();
            } else {
                swal("Cancelled", "Your action is cancelled", "error");
            }
        });
}