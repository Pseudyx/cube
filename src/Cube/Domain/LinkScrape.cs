﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cube.Domain
{
    public class LinkScrape
    {
        public string Url { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Author { get; set; }
        public string Site { get; set; }
        public List<string> Images { get; set; }
    }
}
