﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Cube.Domain;
using HtmlAgilityPack;

namespace Cube.Service
{
    public static class PageScraper
    {
        public static LinkScrape ScrapeSummary(string uri)
        {
            var request = HttpWebRequest.Create(uri) as HttpWebRequest;
            var response = request.GetResponse() as HttpWebResponse;

            var responseStream = response.GetResponseStream();

            var document = new HtmlDocument();
            document.Load(responseStream);

            var linkScrape = new LinkScrape
            {
                Url = uri,
                Images = new List<string>()
            };

            //Check if contain Open graph element 
            var ogMeta = document.DocumentNode.SelectNodes("//meta[@property]");
            if (ogMeta != null)
            {
                var ogImage = ogMeta.Where(x => x.Attributes["property"].Value == "og:image");

                if (ogImage.Any())
                {
                    linkScrape.Images.AddRange(ogImage.Select(x => x.Attributes["content"].Value));
                    linkScrape.Images.AddRange(CheckImgsPath(GetImages(document.DocumentNode.SelectNodes("//img")), uri));
                }
                else linkScrape.Images.AddRange(GetImages(document.DocumentNode.SelectNodes("//img")));    

                var ogTitle = ogMeta.Where(x => x.Attributes["property"].Value == "og:title");
                linkScrape.Title = ogTitle.Any() 
                    ? ogTitle.FirstOrDefault().Attributes["content"].Value 
                    : document.DocumentNode.SelectNodes("//title").Select(x => x.InnerText).FirstOrDefault();

                var ogDescription = ogMeta.Where(x => x.Attributes["property"].Value == "og:description");
                linkScrape.Summary = ogDescription.Any()
                    ? ogDescription.FirstOrDefault().Attributes["content"].Value
                    : (document.DocumentNode.SelectNodes("//p") != null)
                        ? document.DocumentNode.SelectNodes("//p").Select(x => x.InnerText)
                            .FirstOrDefault()
                        : null;

                var ogSite = ogMeta.Where(x => x.Attributes["property"].Value == "og:site_name");
                if (ogSite.Any()) //check og:site_name found 
                    linkScrape.Site = ogSite.FirstOrDefault().Attributes["content"].Value;

                var articleAuthor = ogMeta.Where(x => x.Attributes["property"].Value == "article:author");
                if (articleAuthor.Any()) //check article:author found 
                    linkScrape.Author = articleAuthor.FirstOrDefault().Attributes["content"].Value;

            }
            else
            {
                linkScrape.Images.AddRange(CheckImgsPath(GetImages(document.DocumentNode.SelectNodes("//img")), uri));
                linkScrape.Title = document.DocumentNode.SelectNodes("//title").Select(x => x.InnerText).FirstOrDefault();
                linkScrape.Summary = (document.DocumentNode.SelectNodes("//p") != null)
                        ? document.DocumentNode.SelectNodes("//p").Select(x => x.InnerText)
                            .FirstOrDefault()
                        : null;
            }

            return linkScrape;
        }

        private static IEnumerable<string> GetImages(IEnumerable<HtmlNode> nodes)
        {
            var images = new List<string>();
            if (nodes != null)
            {
                images.AddRange(nodes.Select(img => img.Attributes["src"].Value));
            }
            return images;
        }

        private static string GetHtmlImages(IEnumerable<HtmlNode> nodes) 
        { 
            var Images = new StringBuilder(); 
            if (nodes != null) 
            { 
                foreach (var img in nodes) 
                { 
 
 
                    Images.AppendFormat("<li>"); 
                    Images.AppendFormat(img.OuterHtml); 
                    Images.AppendFormat("</li>"); 
 
                } 
            } 
            return Images.ToString();
        }

        private static IEnumerable<string> CheckImgsPath(IEnumerable<string> images, string uri)
        {
            var checkImgs = new List<string>();
            var url = new Uri(uri);
            
            if (images != null)
            {
                //images.ToList().ForEach(img =>
                foreach (var img in images)
                {
                    if (!img.Contains("http"))
                    {
                        if (img.IndexOf("//", System.StringComparison.Ordinal) == 0) checkImgs.Add(url.Scheme + ":" + img);
                        else if (img.IndexOf('/') > 0) checkImgs.Add(url.Scheme+ "://" + url.DnsSafeHost + "/" + img);
                        else if (img.IndexOf('/') > -1 && img.IndexOf('/') != 0) checkImgs.Add(url.Scheme + "://" + url.DnsSafeHost + "/" + img);
                        else if (img.IndexOf('/') == -1) checkImgs.Add(url.Scheme + "://" + url.DnsSafeHost + url.AbsolutePath.Substring(0, url.AbsolutePath.LastIndexOf('/') + 1) + img);
                        else checkImgs.Add(url.Scheme + "://" + url.DnsSafeHost + img);
                    }
                    else checkImgs.Add(img);
                }
                //);
            }
            return checkImgs;
        }

    }
}
